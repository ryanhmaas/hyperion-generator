#Update Gem File first to get necessary assets in pipeline
update_gem_file

#Create Static Pages
if yes?("Would you like to use static pages?")
	create_static_pages
end

#Get name of client
@client_name = ask("What is the name of the client?")
init_active_admin(@client_name)

#Init AA^2

def source_paths
	[File.expand_path(File.dirname(__FILE__))]
end

def update_gem_file
	#adds AA, Arctic Admin, Devise, Bootsnap, Mailgun
	gem "active_admin"
	gem "arctic_admin"
	gem "devise"
	gem "bootsnap"
	gem "mailgun-ruby"

	#remove Sqlite as default DB
	comment_lines 'Gemfile', '/gem sqlite3/'

	#Add Postgres & HerokuDeflater for prod environments
	gem_group :production do
		gem "pg"
		gem "heroku-deflater"
	end

	gem_group :development do
	  gem 'sqlite3'
	  gem "better_errors"
	  gem "binding_of_caller"
	end

	#install dependencies
	run "bundle install"	
end

#Inits AA for the CMS
def init_active_admin(client)
	generate 'active_admin:install'
	rails_command 'db:migrate'
	rails_command 'db:seed'

	#edit client name - TODO write regex for line
	gsub_file('config/initializers.rb', 'config.site_title=', 'config.site_title = #{client}')
end

def init_arctic_admin
	insert_into_file('app/assets/stylesheets/active_admin.scss', 
	'\n @import "active_admin/mixins"; \n @import "active_admin/base";',
	 :after => "$sidebar-width: 242px;" )
end

#adds all models to AA resources
def add_active_admin_resources
	Models.each do |model| 
		generate 'active_admin:resource #{model}'
	end
end	


#Inits Devise
def init_devise_views
	generate 'devise:views'
end	

#creates static pages
def create_static_pages
end	